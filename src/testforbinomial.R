## test
n=100
data <- matrix(NaN, nrow=n+1, ncol=6)
wT <- data[,1]
aT <- data[,2]
wTp <- data[,3]
aTp <- data[,4]
aTphat <- data[,5]
wTphat <- data[,6]

sigma <- 0.5
miu <- 0.1
r <- 0.05
wt <- 80

u <- exp(sigma*sqrt(1/n))
d <- 1/u
phat <- (exp(r*1/n)-d)/(u-d)
p <- (exp(miu*1/n)-d)/(u-d)

for (k in 0:n){     
  wT[k+1] <- wt*(u^k)*(d^(n-k))
  wTp[k+1] <- dbinom(k,n,p)*wT[k+1]
  aT[k+1] <- max(100-wT[k+1],0)
  aTp[k+1] <- dbinom(k,n,p)*aT[k+1] 
  aTphat[k+1] <- dbinom(k,n,phat)*aT[k+1]  
  wTphat[k+1] <- dbinom(k,n,phat)*wT[k+1] 
}
  epwT <- sum(wTp)
  epaT <- sum(aTp)
  ephataT <- sum(aTphat)
  ephatwT <- sum(wTphat)
  at <- ephataT/exp(r)

#beta <- cov(wT, aT)/var(wT)

#atstar <- (epaT-beta*(epwT-exp(r)*wt))/exp(r)

betaup <- 0
betadown <- 0
for (k in 0:n) {
  betaupk <- dbinom(k,n,p)*(wT[k+1]-epwT)*(aT[k+1]-epaT)
  betaup <- betaup+betaupk  
  betadownk <- dbinom(k,n,p)*((wT[k+1]-epwT)^2)
  betadown <- betadown+betadownk
}
betadollar <- betaup/betadown
atstar1 <- (epaT-betadollar*(epwT-exp(r)*wt))/exp(r)

