library(openxlsx)
library(plyr)
library(ggplot2)
load('data/temp/stage3.RData')

LD_mean <- ddply(data, 'date', function(x)c(meanLD = mean(x$LD, na.rm = T)))
write.xlsx(LD_mean, 'data/exportLD/mean.LD.xlsx')

colnames(data)[39] <- 'Liquid.Discount'
data$date <- format(data$date, '%y-%m')

for(i in 1:length(unique(data$code))){
  sub_data <- data[data$code == unique(data$code)[i],c('date', 'Liquid.Discount')]
  sub_data <- sub_data[complete.cases(sub_data),]
  sub_data <- as.ts(sub_data)
  write.xlsx(sub_data, paste0('data/exportLD/firm', i, '.xlsx'))
}