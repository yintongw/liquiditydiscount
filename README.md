# 基于中国上市公司的流动性折价研究

## 摘要

在投资理财、兼并收购等业务活动中，公司价值的评估至关重要，而流动性对公司资产的影响不容忽视，因此，合理估计流动性风险，科学评估资产价值具有重要的理论及实际意义。然而现有文献大多研究可以定性描述流动性的风险指标与资产收益的相关性，却因采取的计量方法差异导致实证结果不同，并缺乏关于资产流动性对资产定价影响的定量研究。

本文基于我国上市公司数据，运用Geske（1979）复合期权定价模型和Chen（2012）流动性折价模型，估计公司资产流动性折价，拟通过定量研究合理评估考虑流动性风险的资产价值。研究发现，我国上市公司流动性风险较大，债务比例较高。按行业分类，资产流动性具有明显的行业差异，并受到宏观政策的影响。按板块分类，主板上市公司的平均流动性折价水平略高于中小企业板与创业板，可能受其行业组成差异与交易性质差异所致。此外，本文将流动性折价与部分公司指标（如净资产收益率、资产负债率、公司规模等）进行回归，研究发现，公司的流动性折价受公司的债务结构及规模所影响，负债比例越大，公司规模越小，流动性风险越高。

## Abstract

Valuation of the company is crucial to investment decision-making, mergers, acquisitions and other business activities, and the impact of liquidity on the company assets valuation cannot be ignored. Therefore, estimation of the liquidity risk is important both for theoretical exploration and for practical research. However, most of the existing literature just describes the correlation between risk index and return on assets, but they may derive different results due to differences in measurement methods. Moreover, little is known about how liquidity risk affects the asset valuation in a quantitative way. 

In this paper, we estimate the liquidity discount of China's listing Corporation based on Chinese stock market, through a combination of Geske (1979) compound option pricing model and Chen (2012) liquidity discount model. We find that the liquidity discount in Chinese market is large comparing to Taiwan’s stock market. 

According to the industry classification, liquidity discount shows obvious differences between different industries, and it is influenced by the macro economic policy. According to the plate classification, the average liquidity discount level on Main Board Market is slightly higher than that of the SME（Small and Medium Enterprises）Board and the GEM(Growth Enterprises Market) Board, which may be caused by differences in industry composition and the nature of transactions. In addition, this paper discusses the influence factor of liquidity discount through panel regression and we find that liquidity discount can be partly captured by debt structure and scale effect, i.e. the greater proportion of debt or the smaller size of firm can contribute to the risk of liquidity.


## 关键词

流动性折价；资产定价；复合期权

Liquidity Discount;  Asset Pricing;  Compound Option.